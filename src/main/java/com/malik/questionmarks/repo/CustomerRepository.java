package com.malik.questionmarks.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.malik.questionmarks.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
    List<Customer> findByLastName(String lastName);

}
